package gemtext

// Heading represents a heading in gemtext.
type Heading struct {
	Level   int
	Display string
} //struct
