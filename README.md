# gemtext
Utilities for reading and writing gemtext documents.
https://portal.mozz.us/gemini/gemini.circumlunar.space/docs/gemtext.gmi

## Copyright
Licensed under the terms of the MIT License. Please see LICENSE file for more
information.