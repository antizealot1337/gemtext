package gemtext

// Link represents a link in gemtext.
type Link struct {
	URL     string
	Display string
} //struct
