package gemtext

import (
	"bufio"
	"io"
	"strings"
)

// Reader is used to read gemtext documents.
type Reader struct {
	r *bufio.Reader
} //struct

// NewReader creates a new Reader.
func NewReader(r io.Reader) *Reader {
	return &Reader{
		r: bufio.NewReader(r),
	}
} //func

// Line reads a line of the document and returns it.
func (r *Reader) Line() (Line, error) {
	line, err := r.r.ReadString('\n')
	if err != nil {
		return nil, err
	} //if

	line = strings.TrimSpace(line)

	switch {
	case line == "```":
		return Blockquote{}, nil
	case strings.HasPrefix(line, "######"):
		const len = 6
		heading := Heading{
			Level:   len,
			Display: strings.TrimSpace(line[len:]),
		}
		return heading, nil
	case strings.HasPrefix(line, "#####"):
		const len = 5
		heading := Heading{
			Level:   len,
			Display: strings.TrimSpace(line[len:]),
		}
		return heading, nil
	case strings.HasPrefix(line, "####"):
		const len = 4
		heading := Heading{
			Level:   len,
			Display: strings.TrimSpace(line[len:]),
		}
		return heading, nil
	case strings.HasPrefix(line, "####"):
		const len = 3
		heading := Heading{
			Level:   len,
			Display: strings.TrimSpace(line[len:]),
		}
		return heading, nil
	case strings.HasPrefix(line, "###"):
		const len = 3
		heading := Heading{
			Level:   len,
			Display: strings.TrimSpace(line[len:]),
		}
		return heading, nil
	case strings.HasPrefix(line, "##"):
		const len = 2
		heading := Heading{
			Level:   len,
			Display: strings.TrimSpace(line[len:]),
		}
		return heading, nil
	case strings.HasPrefix(line, "#"):
		const len = 1
		heading := Heading{
			Level:   len,
			Display: strings.TrimSpace(line[len:]),
		}
		return heading, nil
	case strings.HasPrefix(line, "*"):
		li := ListItem{
			Display: strings.TrimSpace(line[1:]),
		}
		return li, nil
	case strings.HasPrefix(line, "=>"):
		var link Link
		line = strings.TrimSpace(line[2:])
		idx := strings.IndexRune(line, ' ')
		if idx == -1 {
			link.URL = line
		} else {
			link.URL = line[:idx]
			link.Display = strings.TrimSpace(line[idx+1:])
		} //if
		return link, nil
	default:
		return Text{line}, nil
	} //switch
} //func
