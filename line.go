package gemtext

// Line is a line in a gemtext document. It can be any of Blockquote, Heading,
// Link, ListItem, or Text.
type Line interface{}
