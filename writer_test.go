package gemtext

import (
	"bytes"
	"fmt"
	"io"
	"testing"
)

func TestNewWriter(t *testing.T) {
	var buff bytes.Buffer

	w := NewWriter(&buff)

	if w == nil {
		t.Error("expected w to not be nil")
	} //if

	if w.w == nil {
		t.Error("expected w.w to not be nil")
	} //if
} //func

func TestWriterText(t *testing.T) {
	var buff bytes.Buffer

	w := NewWriter(&buff)

	w.Text("this is some text")

	if expected, actual := "this is some text\n", buff.String(); actual != expected {
		t.Errorf(`expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestWriteLink(t *testing.T) {
	t.Run("TestWithDisplay", func(t *testing.T) {
		var buff bytes.Buffer

		w := NewWriter(&buff)

		w.Link("gemini://example.org/", "Example")

		if expected, actual := "=> gemini://example.org/ Example\n", buff.String(); actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
	t.Run("TestWithoutDisplay", func(t *testing.T) {
		var buff bytes.Buffer

		w := NewWriter(&buff)

		w.Link("gemini://example.org/", "")

		if expected, actual := "=> gemini://example.org/\n", buff.String(); actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
} //func

func TestWriterH1(t *testing.T) {
	var buff bytes.Buffer

	w := NewWriter(&buff)

	w.H1("heading")

	if expected, actual := "# heading\n", buff.String(); actual != expected {
		t.Errorf(`expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestWriterH2(t *testing.T) {
	var buff bytes.Buffer

	w := NewWriter(&buff)

	w.H2("heading")

	if expected, actual := "## heading\n", buff.String(); actual != expected {
		t.Errorf(`expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestWriterH3(t *testing.T) {
	var buff bytes.Buffer

	w := NewWriter(&buff)

	w.H3("heading")

	if expected, actual := "### heading\n", buff.String(); actual != expected {
		t.Errorf(`expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestWriterH4(t *testing.T) {
	var buff bytes.Buffer

	w := NewWriter(&buff)

	w.H4("heading")

	if expected, actual := "#### heading\n", buff.String(); actual != expected {
		t.Errorf(`expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestWriterH5(t *testing.T) {
	var buff bytes.Buffer

	w := NewWriter(&buff)

	w.H5("heading")

	if expected, actual := "##### heading\n", buff.String(); actual != expected {
		t.Errorf(`expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestWriterH6(t *testing.T) {
	var buff bytes.Buffer

	w := NewWriter(&buff)

	w.H6("heading")

	if expected, actual := "###### heading\n", buff.String(); actual != expected {
		t.Errorf(`expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestWriterList(t *testing.T) {
	var buff bytes.Buffer

	w := NewWriter(&buff)

	w.List("item 1", "item 2")

	if expected, actual := "* item 1\n* item 2\n", buff.String(); actual != expected {
		t.Errorf(`expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestBlockqoute(t *testing.T) {
	t.Run("TestSuccess", func(t *testing.T) {
		var buff bytes.Buffer

		w := NewWriter(&buff)

		err := w.Blockquote(func(i io.Writer) error {
			_, err := fmt.Fprintln(i, "hello")
			if err != nil {
				return err
			} //if

			_, err = fmt.Fprintln(i, "world")
			if err != nil {
				return err
			} //if

			return nil
		})
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if expected, actual := "```\nhello\nworld\n```\n", buff.String(); actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
	t.Run("TestReturnError", func(t *testing.T) {
		var buff bytes.Buffer

		w := NewWriter(&buff)

		err := w.Blockquote(func(i io.Writer) error {
			return fmt.Errorf("err")
		})

		if err == nil {
			t.Error("expected error from call to Blockquote")
		} //if
	})
} //func
