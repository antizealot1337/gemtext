package gemtext

import (
	"bufio"
	"bytes"
	"strings"
	"testing"
)

func TestNewReader(t *testing.T) {
	r := NewReader(bytes.NewBufferString(""))

	if r == nil {
		t.Fatal("expected r to not be nil")
	} //if

	if r.r == nil {
		t.Fatal("expected r.r to not be nil")
	} //if
} //func

func TestReaderLine(t *testing.T) {
	t.Run("TestBlockquote", func(t *testing.T) {
		r := Reader{
			r: bufio.NewReader(strings.NewReader("```\n")),
		}

		line, err := r.Line()
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		_, ok := line.(Blockquote)
		if !ok {
			t.Fatal("expected Line to be Blockquote")
		} //if
	})
	t.Run("TestH1", func(t *testing.T) {
		r := Reader{
			r: bufio.NewReader(strings.NewReader("# heading\n")),
		}

		line, err := r.Line()
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		heading, ok := line.(Heading)
		if !ok {
			t.Fatal("expected Line to be Heading")
		} //if

		if expected, actual := 1, heading.Level; actual != expected {
			t.Errorf("expected level %d but was %d", expected, actual)
		} //if

		if expected, actual := "heading", heading.Display; actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
	t.Run("TestH2", func(t *testing.T) {
		r := Reader{
			r: bufio.NewReader(strings.NewReader("## heading\n")),
		}

		line, err := r.Line()
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		heading, ok := line.(Heading)
		if !ok {
			t.Fatal("expected Line to be Heading")
		} //if

		if expected, actual := 2, heading.Level; actual != expected {
			t.Errorf("expected level %d but was %d", expected, actual)
		} //if

		if expected, actual := "heading", heading.Display; actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
	t.Run("TestH3", func(t *testing.T) {
		r := Reader{
			r: bufio.NewReader(strings.NewReader("### heading\n")),
		}

		line, err := r.Line()
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		heading, ok := line.(Heading)
		if !ok {
			t.Fatal("expected Line to be Heading")
		} //if

		if expected, actual := 3, heading.Level; actual != expected {
			t.Errorf("expected level %d but was %d", expected, actual)
		} //if

		if expected, actual := "heading", heading.Display; actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
	t.Run("TestH4", func(t *testing.T) {
		r := Reader{
			r: bufio.NewReader(strings.NewReader("#### heading\n")),
		}

		line, err := r.Line()
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		heading, ok := line.(Heading)
		if !ok {
			t.Fatal("expected Line to be Heading")
		} //if

		if expected, actual := 4, heading.Level; actual != expected {
			t.Errorf("expected level %d but was %d", expected, actual)
		} //if

		if expected, actual := "heading", heading.Display; actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
	t.Run("TestH5", func(t *testing.T) {
		r := Reader{
			r: bufio.NewReader(strings.NewReader("##### heading\n")),
		}

		line, err := r.Line()
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		heading, ok := line.(Heading)
		if !ok {
			t.Fatal("expected Line to be Heading")
		} //if

		if expected, actual := 5, heading.Level; actual != expected {
			t.Errorf("expected level %d but was %d", expected, actual)
		} //if

		if expected, actual := "heading", heading.Display; actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
	t.Run("TestH6", func(t *testing.T) {
		r := Reader{
			r: bufio.NewReader(strings.NewReader("###### heading\n")),
		}

		line, err := r.Line()
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		heading, ok := line.(Heading)
		if !ok {
			t.Fatal("expected Line to be Heading")
		} //if

		if expected, actual := 6, heading.Level; actual != expected {
			t.Errorf("expected level %d but was %d", expected, actual)
		} //if

		if expected, actual := "heading", heading.Display; actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
	t.Run("TestListItem", func(t *testing.T) {
		r := Reader{
			r: bufio.NewReader(strings.NewReader("* list item\n")),
		}

		line, err := r.Line()
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		li, ok := line.(ListItem)
		if !ok {
			t.Fatal("expected Line to be Link")
		} //if

		if expected, actual := "list item", li.Display; actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
	t.Run("TestLink", func(t *testing.T) {
		r := Reader{
			r: bufio.NewReader(strings.NewReader("=> gemini://example.org\n")),
		}

		line, err := r.Line()
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		link, ok := line.(Link)
		if !ok {
			t.Fatal("expected Line to be Link")
		} //if

		if expected, actual := "gemini://example.org", link.URL; actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if

		if expected, actual := "", link.Display; actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
	t.Run("TestLinkWithDisplay", func(t *testing.T) {
		r := Reader{
			r: bufio.NewReader(strings.NewReader("=> gemini://example.org this is a good link\n")),
		}

		line, err := r.Line()
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		link, ok := line.(Link)
		if !ok {
			t.Fatal("expected Link to be Link")
		} //if

		if expected, actual := "gemini://example.org", link.URL; actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if

		if expected, actual := "this is a good link", link.Display; actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
	t.Run("TestTextLine", func(t *testing.T) {
		r := Reader{
			r: bufio.NewReader(strings.NewReader("this is a line\n")),
		}

		line, err := r.Line()
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		text, ok := line.(Text)
		if !ok {
			t.Fatal("expected Line to be Text")
		} //if

		if expected, actual := "this is a line", text.Display; actual != expected {
			t.Errorf(`expected "%s" but was "%s"`, expected, actual)
		} //if
	})
} //func
