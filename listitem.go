package gemtext

// ListItem represents a list item in gemtext.
type ListItem struct {
	Display string
} //struct
