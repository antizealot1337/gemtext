package gemtext

import (
	"fmt"
	"io"
)

// Writer is used to write a gemtext document.
type Writer struct {
	w io.Writer
} //struct

// NewWriter creates a new Writer.
func NewWriter(w io.Writer) *Writer {
	return &Writer{
		w: w,
	}
} //func

// Text will write a line of text.
func (w *Writer) Text(line string) error {
	_, err := fmt.Fprintln(w.w, line)
	return err
} //func

// Link writes a link. If display is empty ("") then the display will be omitted
// from the write.
func (w *Writer) Link(url, display string) error {
	if display == "" {
		_, err := fmt.Fprintln(w.w, "=>", url)
		return err
	} //if

	_, err := fmt.Fprintln(w.w, "=>", url, display)
	return err
} //func

// H1 writes a first level header.
func (w *Writer) H1(heading string) error {
	_, err := fmt.Fprintln(w.w, "#", heading)
	return err
} //func

// H2 writes a second level header.
func (w *Writer) H2(heading string) error {
	_, err := fmt.Fprintln(w.w, "##", heading)
	return err
} //func

// H3 writes a third level header.
func (w *Writer) H3(heading string) error {
	_, err := fmt.Fprintln(w.w, "###", heading)
	return err
} //func

// H4 writes a fourth level header.
func (w *Writer) H4(heading string) error {
	_, err := fmt.Fprintln(w.w, "####", heading)
	return err
} //func

// H5 writes a fifth level header.
func (w *Writer) H5(heading string) error {
	_, err := fmt.Fprintln(w.w, "#####", heading)
	return err
} //func

// H6 writes a sixth level header.
func (w *Writer) H6(heading string) error {
	_, err := fmt.Fprintln(w.w, "######", heading)
	return err
} //func

// List writes items as a list.
func (w *Writer) List(items ...string) error {
	for _, line := range items {
		_, err := fmt.Fprintln(w.w, "*", line)
		if err != nil {
			return err
		} //if
	} //for

	return nil
} //func

// Blockquote initiates a blockquote section. The provided function writes
// directly to the underlying Writer. Any errors from the provided function
// will be returned from this function.
func (w *Writer) Blockquote(f func(io.Writer) error) error {
	_, err := fmt.Fprintln(w.w, "```")
	if err != nil {
		return err
	} //if

	err = f(w.w)
	if err != nil {
		return err
	} //if

	_, err = fmt.Fprintln(w.w, "```")
	return err
} //func
